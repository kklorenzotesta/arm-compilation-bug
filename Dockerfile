FROM ubuntu:20.04

RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y wget
RUN mkdir -p /toolchain-deps/gnuaet && \
        wget -q -O- https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2 | tar xjf - -C /toolchain-deps/gnuaet --strip-components=1
ENV PATH="/toolchain-deps/gnuaet/bin:${PATH}"
RUN apt-get install -y unzip
RUN wget -q http://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v17.x.x/nRF5_SDK_17.0.2_d674dde.zip && unzip -q nRF5_SDK_17.0.2_d674dde.zip -d /toolchain-deps/sdk && mv /toolchain-deps/sdk/nRF5_SDK_17.0.2_d674dde/* /toolchain-deps/sdk/ && rm nRF5_SDK_17.0.2_d674dde.zip
ENV NRF52_SDK_ROOT="/toolchain-deps/sdk"
RUN apt-get install -y make
RUN apt-get install -y srecord
RUN wget -q https://www.nordicsemi.com/-/media/Software-and-other-downloads/Desktop-software/nRF-command-line-tools/sw/Versions-10-x-x/10-12-1/nRFCommandLineTools10121Linuxamd64.tar.gz && \
        mkdir tmpTools && \
        tar -xzvf nRFCommandLineTools10121Linuxamd64.tar.gz -C /tmpTools && \
        cd tmpTools && tar -xzvf nRF-Command-Line-Tools_10_12_1.tar -C /toolchain-deps && cd .. && \
        rm -r /tmpTools && rm nRFCommandLineTools10121Linuxamd64.tar.gz
RUN apt-get install -y libncurses5
ENV PATH="/toolchain-deps/mergehex:${PATH}"
COPY . /contiki-uwb
ENV UWB_CONTIKI="/contiki-uwb"
